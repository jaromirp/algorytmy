package tablica;

import java.util.Random;

public class Sortowanie {
    //  package pl.sda.algorytmy.sortowanie.selection;

//import java.util.Random;

    //public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random random = new Random();

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(500);
        }

        wypisz(tablica);

        sortSelection(tablica);
        System.out.println();

        wypisz(tablica);
    }

    private static void sortSelection(int[] tablica) {
        int temp;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = i + 1; j < tablica.length; j++) {
                if (tablica[i] > tablica[j]) {// < - odwrócona tablica
                    temp = tablica[i];
                    tablica[i] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
    }

    private static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
    }
}
