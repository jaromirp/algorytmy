package tablica;

//Zadanie Tworzenie Tablicy
//Po stworzeniu Tablicy Generowanie dowolnych liczb w tablicy przewidziano 20 liczb (int)
//Należy posortować

//SORTOWANIE 10.07.2018

import java.util.Arrays;
import java.util.Random;

public class Tablica {
    public static void main(String[] args) {

        int[] tablica = new int[20]; //Tablica 6 elementów
        Random generator = new Random();

        for (int i = 0; i < tablica.length; i++) {

            int b = generator.nextInt();
            tablica[i] = b; //przypisanie do i-tego elementu tablicy wartość b

            System.out.print(b); //Wyświetlenie tylko jednego wylosowanego elementu

        }
        System.out.println();
        //for (int j = 0; j < tablica[i].length; j++)
        //tablica[i][j] = 0;
        //tablica[i] = tablica.length;
        selectionSort(tablica); // przekazanie do funkcji całej tablicy

        System.out.println(Arrays.toString(tablica));
    }

    public static void selectionSort(int[] arrayToSort) {



        for (int i = 0; i < arrayToSort.length; i++) {
            int minimalnyIndex = i;
            for (int j = i + 1; j < arrayToSort.length; j++) {
                if (arrayToSort[j] < arrayToSort[minimalnyIndex]) {
                    minimalnyIndex = j;
                }

            }
            int tmp = arrayToSort[minimalnyIndex];
            arrayToSort[minimalnyIndex] = arrayToSort[i];
            arrayToSort[i] = tmp;

            //System.out.println();
        }
    }
}
