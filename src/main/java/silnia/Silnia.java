package silnia;

//ALGORYTMY   SILNIA
//REKURENCJA

public class Silnia {
    public static void main(String[] args) {

        System.out.println(silnia(6));
        System.out.println(silnia2(5));

    }

    static int silnia(int a) {   //1 metoda TRadycyjna
        int silnia = 1;
        for (int i = 1; i <= a; i++) {

            silnia *= i;
        }
        return silnia;

    }

    static int silnia2(int a) {
        return a == 1 ? a : a * silnia2(a - 1);   //2Metoda rekurencyjna SILNIA  --> to jest IF


    }
}


